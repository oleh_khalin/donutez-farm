export const navItems = [
  {
    link: '/',
    label: 'Create Farming',
  },
  {
    link: '/deploy',
    label: 'Deploy Token',
  },
  {
    link: '/farm',
    label: 'Farm DONUTEZ',
  },
  {
    link: '/account',
    label: 'Account',
  },
];
