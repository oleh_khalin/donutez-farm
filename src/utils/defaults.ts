export const SEO = {
  DEFAULT_TITLE: 'DONUTEZ FARM | Create your own yeild farming in 5 minutes',
  DEFAULT_DESSCRIPTION: 'DEFAULT DESCRIPTION',
  DEFAULT_IMAGE: 'og_image.jpg',
  TEMPLATE_TITLE: 'DONUTEZ FARM',
  WEBSITE_URL: 'https://donutez-farm.vercel.app/', // Slash in the end is necessary
  CREATOR: '@donutez',
};

export enum NetworkType {
  MAINNET = 'mainnet',
  DELPHINET = 'delphinet',
  EDONET = 'edonet',
  CUSTOM = 'custom',
}

export const APP_NAME = 'DONUTEZ FARM';

export const NETWORK = NetworkType.DELPHINET;
export const NETWORK_RPC = 'https://api.tez.ie/rpc/delphinet';

export const CONSTRUCT_FARM_CONTRACT = 'KT1G4Lj716mUf9FCN9RMKp4871JgoY6SmE53';
export const CONSTRUCT_FEE = 10;
export const CONSTRUCT_STAKE_FEE = 5;
export const CONSTRUCT_STAKE_SUM = 7;

export const FARM_CONTRACT = 'KT1U2hLtjFQXPTRHJoCguXu5tyTq9gotQ5cy';

export const DTZ_TOKEN = 'KT1NGECsbn7AEPkoJTsJQvE4EonjKAUHEoKL';

export const TOKEN_FA1 = 'KT1UTsV7kUeqJv4duUGH1TxYsN7tiLTUfstT';
export const TOKEN_FA2 = 'KT1JLDYtvCKnT6QaLYAHHr9wH6H7ZnL9tteq';

export const CONTRACT_REWARD = 'KT1U2hLtjFQXPTRHJoCguXu5tyTq9gotQ5cy';
export const CONTRACT_POOL = 'KT1VTrqrKHp8a6ZZEQKZk4pPnUoDLALCAPJL';

export const BACKEND_URL = 'https://sleepy-gorge-06414.herokuapp.com/core';
